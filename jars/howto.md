Gradle Repository


```groovy
repositories {    
    ivy {
    	url "https://bitbucket.org/theevora/public/raw/master/jars/"
    	layout "pattern" , {
    		artifact "[artifact]_[revision].[ext]"
    	}
    }
}
```

Maven Repository
```groovy
repositories {
	maven {
        url = 'https://bitbucket.org/theevora/public/raw/master/jars'
	}
}
```
```kotlin
repositories {
	maven {
        url = uri("https://bitbucket.org/theevora/public/raw/master/jars")
	}
}
```

kmlogger 
  <groupId>th.in.thvo.kmlogger</groupId>
  <artifactId>kmlogger</artifactId>
  <version>1.3.3</version>
  
  "th.in.thvo.kmlogger:kmlogger:1.3.3"